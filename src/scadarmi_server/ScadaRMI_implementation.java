/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scadarmi_server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafish.clients.opc.JOpc;
import javafish.clients.opc.component.OpcGroup;
import javafish.clients.opc.component.OpcItem;
import javafish.clients.opc.exception.ComponentNotFoundException;
import javafish.clients.opc.exception.ConnectivityException;
import javafish.clients.opc.exception.SynchReadException;
import javafish.clients.opc.exception.SynchWriteException;
import javafish.clients.opc.exception.UnableAddGroupException;
import javafish.clients.opc.exception.UnableAddItemException;
import javafish.clients.opc.exception.UnableRemoveGroupException;
import javafish.clients.opc.variant.Variant;

/**
 *
 * @author Administrador
 */
public class ScadaRMI_implementation extends UnicastRemoteObject implements scadarmi_interface.scadaRMI_interface{
    
     JOpc opcServer;
     private OpcItem tagSP;
     private OpcItem tagPV;
     private OpcItem tagMV;
     private OpcItem tagModo;
     OpcItem tagMVMan;
     OpcGroup grupo;
     OpcGroup resposta;
     OpcItem resp;
    
    public ScadaRMI_implementation() throws RemoteException{
        
            
        }
    
    
    @Override
    public boolean ConnectOPCServer(String host, String serverID) throws RemoteException {
        boolean status = false;
        JOpc.coInitialize();
        opcServer = new JOpc(host,serverID, "OPC1");
        
        tagSP = new OpcItem("tagSP", true, "");
        tagPV = new OpcItem("tagPV", true, "");
        tagMV = new OpcItem("tagMV", true, "");
        tagModo = new OpcItem("tagModo", true, "");
        
        grupo = new OpcGroup("grupo1", true, 500, 0.0f);
        
        grupo.addItem(tagSP);
        grupo.addItem(tagPV);
        grupo.addItem(tagMV);
        grupo.addItem(tagModo);
        
        opcServer.addGroup(grupo);
        
        try {
            opcServer.connect();
            opcServer.registerGroups();
            status = true;
        } catch (ConnectivityException ex) {
            status = false;
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnableAddGroupException | UnableAddItemException ex) {
            Logger.getLogger(ScadaRMI_implementation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return status;  
    }

    @Override
    public boolean DesconnectOPCServer() throws RemoteException {
        boolean status = false;
        try {
            opcServer.unregisterGroups();
            status = true;
        } catch (UnableRemoveGroupException ex) {
            status = false;
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
        JOpc.coUninitialize();
        return status;
       
    }

    @Override
    public Double[] read_tag() throws RemoteException {
        //implementacao
        Double[] values = new Double[4];
        try {
                resposta = opcServer.synchReadGroup(grupo);
                for (int i = 0; i <4; i++) {
                    values[i] = resposta.getItems().get(i).getValue().getDouble();  
                }
                
                //OpcItem resp = opcServer.synchReadItem(grupo, tag);
        } catch (ComponentNotFoundException | SynchReadException ex) {
            Logger.getLogger(ScadaRMI_implementation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return values;
    }

    @Override
    public void write_tag(OpcItem tag, double value) throws RemoteException {
        tag.setValue(new Variant(value));
        try {
            opcServer.synchWriteItem(grupo, tag);
        } catch (ComponentNotFoundException | SynchWriteException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the tagSP
     */
    public OpcItem getTagSP() {
        return tagSP;
    }

    /**
     * @return the tagPV
     */
    public OpcItem getTagPV() {
        return tagPV;
    }

    /**
     * @return the tagMV
     */
    public OpcItem getTagMV() {
        return tagMV;
    }

    /**
     * @return the tagModo
     */
    public OpcItem getTagModo() {
        return tagModo;
    }

    
    
}
